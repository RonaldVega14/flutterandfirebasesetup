// How to use Firebase App Distribution tool
// Steps:
//  1. Inside your project root folder open bash/cmd and run the following command: flutter build apk
//  2. Locate apk created.
//  3. Go to Firebase Console > App Distribution.
//  4. Upload apk in releases section.
//  5. Add testers by email.
//  6. Follow the instructions on the email received and your good to go!.

// How to use firebase Suite.
// This allows us to create local database environments for our projects.
// Steps:
//  1. run the following command: npm install -g firebase-tools (you need to have node on your environment).
//  2. Run firebase init. (This allows us to connect our project to the cloud).
//  3. Allow firebase command to create all the tools that you want to use.
//  4. Then to starrt the emulator run the following command: flutter emulators:start
//  5. After the widget tree is build point your firebase connection to localhost.
//    5.1. See method pointLocalFirebaseInstance();

import 'package:flutter/material.dart';

import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:firebase_performance/firebase_performance.dart';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseAnalytics analytics;

void main() {
  analytics = FirebaseAnalytics();

  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //Firebase Analytics
  //This method is used to declare all the events for Firebase Analytics that you want logs from
  _analyticsEvent() {
    analytics.logGenerateLead();
    print('Event logged');
  }
  //You can always observe to this function in any Navigator method or page by doing
  //navigatoObservers:[
  //   FirebaseAnalyticsObserver(analytics: FirebaseAnalytics()),
  // ]

  //Firebase Performance
  //Add his method to the process that you want to keep performance track and name it as you like to keep record of it in firebase console.
  _perfTrace() async {
    Trace trace = FirebasePerformance.instance.newTrace('cool_trace');
    trace.start();
    await Future.delayed(Duration(seconds: 5));
    trace.stop();
  }

  //Firebase Crashlytics
  //This is an example where any method that throws an error inside you flutter app.
  //This error log will also appear in you firebase console, but this flutter declared errors are consider as non-fatals.
  _crash() {
    throw FlutterError('Thins is only a test error message.');
  }

  //Firebase emulator connection
  // pointLocalFirebaseInstance() {
  //   String host =
  //       '10.0.2.2:8080'; //If not using android device localhost:8080 should work too.
  //   Firestore.instance.settings(
  //     host: host,
  //     sslEnabled: false,
  //     persistenceEnabled: false,
  //   );
  // }

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
